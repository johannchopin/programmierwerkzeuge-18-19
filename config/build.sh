BIN=bin
SRC=src
JAVAROOT="/usr/lib/jvm/java-1.11.0-openjdk-amd64"

clear
echo building java
javac $SRC/Hello.java
echo generating header from class file
javah $SRC/Hello
echo compiling c file
gcc -I $(JAVAROOT)/include -I $(JAVAROOT)/include/linux -c $SRC/Hello.c
echo linking dynamic link library
gcc -Wl,--add-stdcall-alias -shared -o hello.so hello.o
echo running Java programm
java -Djava.library.path=. Hello
echo deleting temporary files
rm Hello.h Hello.o hello.so Hello.class
