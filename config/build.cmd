@echo off
cls
set JAVAROOT=C:\Java\jdk1.8.0_112
echo building java
javac Hello.java
echo generating header from class file
javah Hello
echo compiling c file 
gcc -I %JAVAROOT%\include -I %JAVAROOT%\include\win32 -c Hello.c
echo linking dynamic link library
gcc -Wl,--add-stdcall-alias -shared -o hello.dll hello.o
echo running Java programm
java -Djava.library.path=. Hello
echo deleting temporary files
del Hello.h Hello.o hello.dll Hello.class
