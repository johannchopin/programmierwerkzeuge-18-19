@echo off
cls
echo testing
set JAVAROOT=C:\Java\jdk1.8.0_112
javac Hello.java
javah Hello
gcc -I %JAVAROOT%\include -I %JAVAROOT%\include\win32 -c Hello.c
gcc -Wl,--add-stdcall-alias -shared -o hello.dll hello.o
javac -cp .;.\junit-platform-console-standalone-1.3.1.jar Tester.java 
java -jar junit-platform-console-standalone-1.3.1.jar --scan-class-path --class-path=.

echo deleting temporary files
del Hello.h Hello.o hello.dll Hello.class Tester.class 