# Programmierwerkzeuge-18-19

## TODO
* [x] structure the project
* [x] write the javadoc
* [x] generate a runnable .jar file
* [x] generate an UML diagramm
* [ ] write a makefile
    * [x] clean
    * [x] compile
    * [x] run
    * [ ] test
    * [x] jar
* [ ] set up Maven
    * [x] javadoc
    * [x] testing with jUnit
    * [ ] Fix compilation of C file int o.so

### Check
* [x] generate a documentation with the command line
* [x] run the programm with the command line
* [x] run unit test with the command line --> Maven instead
* [x] delete every generated file (clean)
* [ ] install and run on an other computer --> Not tested

### Additional TODO
* [x] Improve path for handling .so file
#### Alexandre
* [x] debugging with gdb/jdb
* [x] generate an UML diagramm from the command line --> from eclipse instead
#### Johann

#### Improvments to infinity and beyond
* [ ] generic Hello.java like for running every C compiled file

## Memo
* generate javadoc : 
    - UNIX command line : `javadoc -d docs src/*.java`
    - Maven : `mvn site`

* .jar
    * generate : `jar -cvmf src/MANIFEST.MF runnable/helloWorld.jar bin/*`
    * run : `java -jar -Djava.library.path= helloWorld.jar` 
