#include <stdio.h>
#include "Hello.h"

JNIEXPORT jstring JNICALL Java_Hello_sayHello(JNIEnv *env, jobject thisObj)
{
	jstring jstr = (*env)->NewStringUTF(env, "Hello World !");
	return jstr;
}
