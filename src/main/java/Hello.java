import java.io.File;


/**
 * A simple interface with Hello.c file.
 * Call the method sayHello from Hello.c when main called
 * @version 1.0
 */
public class Hello {
    /**
     * bind compiled file with this class
     */
	static {
		System.load(
                new File(System.getProperty("java.library.path") + File.separator + "hello.so")
                    .getAbsolutePath());
	}

    /**
     * native function from C file code
     */
	public native String sayHello();

    /**
     * Call sayHello method and pass any argument
     */
	public static void main(String[] args) {
		System.out.println(new Hello().sayHello());
	}
}
