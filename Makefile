# Config in system
JAVA_HOME="/usr/lib/jvm/java-1.11.0-openjdk-amd64"

# Directories
ROOT_DIR:=$(shell pwd|tr -d ' ') # Executed when needed
CURRENT_DIR=$(shell pwd | tr -d ' ')
SRC=src/main/java
BIN=bin
LIB=src/main/lib
DOC=docs
RUN=runnable

compile-c-code:
	cd $(SRC); javah Hello
	gcc -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux -c $(SRC)/Hello.c -o $(BIN)/Hello.o
	rm $(SRC)/Hello.h
	gcc -shared -o $(BIN)/hello.so $(BIN)/Hello.o
	rm $(BIN)/Hello.o

compile-java-code:
	javac -cp $(LIB)/junit-platform-console-standalone-1.3.1.jar -d $(BIN) $(SRC)/*.java 

compile: compile-c-code compile-java-code

run: compile
	cd $(BIN) && java -Djava.library.path="$(PWD)/$(BIN)" Hello

test: compile
	java -cp $(LIB)/junit-platform-console-standalone-1.3.1.jar -Djava.library.path="$(PWD)/$(BIN)" org.junit.runner.JUnitCore Tester

doc:
	javadoc -d $(DOC) -cp $(LIB)/junit-platform-console-standalone-1.3.1.jar $(SRC)/*.java

cleandocs:
	$(RM) -r docs/*

clean: cleandocs
	$(RM) -r bin/*
	$(RM) -r tar/*

jar: compile
	cp $(BIN)/hello.so $(RUN)/
	cd $(BIN); jar -cvmf ../$(SRC)/MANIFEST.MF ../$(RUN)/helloWorld.jar *

runjar: jar
	java -jar -Djava.library.path=$(RUN) $(RUN)/helloWorld.jar 
